const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TracksSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	album: {
		type: Schema.Types.ObjectId,
		ref: 'Albums',
		required: true
	},
	duration: Number
});

const Tracks = mongoose.model('Tracks', TracksSchema);

module.exports = Tracks;