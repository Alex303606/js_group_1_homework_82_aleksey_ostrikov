const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumsSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	artist: {
		type: Schema.Types.ObjectId,
		ref: 'Artists',
		required: true
	},
	year: Number,
	image: String
});

const Albums = mongoose.model('Albums', AlbumsSchema);

module.exports = Albums;